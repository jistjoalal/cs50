#define _XOPEN_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int main(int argc, char * argv[])
{
    if (argc != 2)
    {
        printf("Usage: ./crypt key\n");
        return 1;
    }
    printf("%s\n", crypt(argv[1], "50"));
}