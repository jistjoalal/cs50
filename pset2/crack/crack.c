#define _XOPEN_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

char nextChar(char _c);

int main(int argc, char * argv[])
{
    // error checking
    if (argc != 2)
    {
        printf("Usage: ./crack hash\n");
        return 1;
    }

    // store hash (from cli) and salt (first 2 chars of hash)
    string hash = argv[1];
    char salt[3];
    memcpy(salt, &hash[0], 2);
    salt[2] = '\0';

    // for each possibility of num. of digits (chars) in key
    for (int digits = 1; digits <= 4; digits++)
    {
        // initialize test key as 'A' * digits
        char key[digits + 1];
        memset(key, 'A', digits);
        key[digits] = '\0';

        // for all possible permutations of key given the number of digits
        for (long i = 1, n = pow(52, digits); i <= n; i++)
        {
            // compare test key hash to target hash
            if (strncmp(crypt(key, salt), hash, 13) == 0)
            {
                // give the result to the user and exit
                printf("%s\n", key);
                return 0;
            }
            // for each digit check for incrementing char
            for (int j = 0, m = digits; j < m; j++)
            {
                // if the permutation index (i) is divisible by 52 ^ digit index (j)
                if (fmod(i,pow(52, j)) == 0)
                {
                    // increment key 1 char at a time in base 52 (A-Z, a-z)
                    // will increment "ones column" every iteration of i when j is 0 (52^0 = 1)
                    // increments "52^j column" when "52^(j-1) column" incremented 52 times (column overflow)
                    key[j] = nextChar(key[j]);
                }
            }
        }
    }
}

// returns the next char in the alphabet
char nextChar(char _c)
{
    if (_c == 'Z')
    {
        return 'a';
    }
    else if (_c == 'z')
    {
        return 'A';
    }
    else
    {
        return _c + 1;
    }
}