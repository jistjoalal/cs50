#include <stdio.h>
#include <cs50.h>
#include <math.h>

void printType(long long _number);
bool validSum(long long _number);
int getDigitAt(long long _number, int _n);
long long getNumber();

int main(void)
{
    long long number = -1;
    while (number < 0)
    {
        number = getNumber();
    }
    printType(number);

    return 0;
}

// Prints the type of credit card if inputted number matches specified conditions
// else prints invalid
void printType(long long _number)
{
    string returnText = "INVALID\n";
    if (validSum(_number))
    {
        if (getDigitAt(_number, 13) == 4 || getDigitAt(_number, 16) == 4)
        {
            returnText = "VISA\n";
        }
        else if (getDigitAt(_number, 15) == 3 && (getDigitAt(_number, 14) == 4
                 || getDigitAt(_number, 14) == 7))
        {
            returnText = "AMEX\n";
        }
        else if (getDigitAt(_number, 16) == 5 && (getDigitAt(_number, 15) > 0
                 && getDigitAt(_number, 15) < 6))
        {
            returnText = "MASTERCARD\n";
        }
    }
    printf("%s", returnText);
}

// returns true if sum has last digit of 0 after Luhn's Algorithm
bool validSum(long long _number)
{
    int sum = 0;
    int numDigits = log10(_number) + 1;
    // every other digit, starting from 2nd to last, moving "left" across the credit card number
    for (int i = 2; i <= numDigits; i += 2)
    {
        int digitDoubled = getDigitAt(_number, i) * 2;
        if (digitDoubled >= 10)
        {
            sum += digitDoubled - 9;
        }
        else
        {
            sum += digitDoubled;
        }
    }
    // every other digit, starting from the last, moving "left"
    for (int i = 1; i <= numDigits; i += 2)
    {
        sum += getDigitAt(_number, i);
    }
    return sum % 10 == 0;
}

// gets digit of number at index n.
// n = column. (1 = 1's place, 2 = 10's place, 3 = 100's place, etc.)
int getDigitAt(long long _number, int _n)
{
    long long lastNDigits = fmod(_number, pow(10, _n));
    long long lastNMinusOneDigits = fmod(_number, pow(10, _n - 1));
    return (lastNDigits - lastNMinusOneDigits) / pow(10, _n - 1);
}

// promts user for credit card number
long long getNumber()
{
    printf("Number: ");
    return get_long_long();
}