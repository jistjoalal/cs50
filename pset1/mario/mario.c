#include <stdio.h>
#include <cs50.h>

void pyramid(int _height);
void printChars(int _n, char _c);
int getHeight();

int main(void)
{
    int height = -1;
    // Prompt user for height of pyramid until a non-negative integer <= 23 is inputted
    while (height < 0 || height > 23)
    {
        height = getHeight();
    }
    // Print pyramid of specified height
    pyramid(height);
}

// Print pyramid of specified height
void pyramid(int _height)
{
    //Print each row at a time with newline at end
    for (int i = 1; i <= _height; i++)
    {
        // spaces + hashes = height. spaces = height - i. i = row index.
        int spaces = _height - i;
        int hashes = _height - spaces;
        // left half
        printChars(spaces, ' ');
        printChars(hashes, '#');
        // 2 space division
        printf("  ");
        // right half
        printChars(hashes, '#');
        printf("\n");
    }
}

// Prints char _c _n times.
void printChars(int _n, char _c)
{
    for (int i = 0; i < _n; i++)
    {
        printf("%c", _c);
    }
}

// Prompts user for height in the form of an integer.
int getHeight()
{
    printf("Height: ");
    return get_int();
}