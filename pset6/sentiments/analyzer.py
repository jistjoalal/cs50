class Analyzer():
    """Implements sentiment analysis."""

    def __init__(self, positives, negatives):
        """Initialize Analyzer."""
        self.positives = list()
        self.negatives = list()
        for line in open("positive-words.txt"):
            # strip whitespace
            li = line.strip()
            # avoid comments and blank lines
            if not li.startswith(";") and line != "\n":
                self.positives.append(li)
        for line in open(negatives):
            li = line.strip()
            if not li.startswith(";") and line != "\n":
                self.negatives.append(li)

    """Analyze text for sentiment, returning its score."""

    def analyze(self, text):
        wordsum = 0
        for word in text.split(" "):
            word = word.lower()
            for pos in self.positives:
                if word == pos:
                    wordsum += 1
                    break
            for neg in self.negatives:
                if word == neg:
                    wordsum -= 1
                    break

        return wordsum
