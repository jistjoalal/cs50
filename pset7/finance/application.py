from cs50 import SQL
from flask import Flask, flash, redirect, render_template, request, session, url_for
from flask_session import Session
from passlib.context import CryptContext
from tempfile import mkdtemp

from helpers import *

# configure application
app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

# initialize encryption context
ctx = CryptContext(schemes=["sha256_crypt", "md5_crypt"])

# ensure responses aren't cached
if app.config["DEBUG"]:
    @app.after_request
    def after_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        response.headers["Expires"] = 0
        response.headers["Pragma"] = "no-cache"
        return response

# custom filter
app.jinja_env.filters["usd"] = usd

# configure session to use filesystem (instead of signed cookies)
app.config["SESSION_FILE_DIR"] = mkdtemp()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# configure CS50 Library to use SQLite database
db = SQL("sqlite:///finance.db")


@app.route("/")
@login_required
def index():
    """Show users stock portfolio"""

    # get users portfolio of stocks
    portfolio = dict()
    rows = db.execute("SELECT * FROM transactions WHERE userID = :userID",
                      userID=session["user_id"])
    for row in rows:
        if row["symbol"] in portfolio:
            portfolio[row["symbol"]] += row["shares"]
        else:
            portfolio[row["symbol"]] = row["shares"]

    # prep portfolio for display
    stocks = list(dict())
    grandTotal = 0
    for symbol, shares in portfolio.items():
        # get current price of this stock
        price = lookup(symbol)['price']
        # calculate total value
        total = price * int(shares)
        grandTotal += total
        stocks.append({'symbol': symbol, 'name': symbol, 'shares': shares,
                       'price': price, 'total': total})

    # get users cash amount and add to grand total
    result = db.execute("SELECT cash FROM users WHERE id = :id",
                        id=session["user_id"])
    if result == None:
        return apology("could not get users cash")
    cash = result[0]['cash']
    grandTotal += cash

    # display portfolio on home page
    return render_template("index.html", stocks=stocks, grandTotal=grandTotal, cash=cash)


@app.route("/buy", methods=["GET", "POST"])
@login_required
def buy():
    """Buy shares of stock."""

    # if user reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # check that symbol is entered
        if not request.form.get("symbol"):
            return apology("must provide stock symbol")

        # check that num of shares entered (positive int)
        if not request.form.get("shares") or int(request.form.get("shares")) < 0:
            return apology("must provide a valid # of shares")

        # get price
        quote = lookup(request.form.get("symbol"))
        if quote == None:
            return apology("could not find price of symbol")

        # get users cash
        result = db.execute("SELECT cash FROM users WHERE id = :id",
                            id=session["user_id"])

        # check that cash amount could be retrieved from db
        if result == None:
            return apology("could not get users cash")

        # check that purchase is possible
        if float(result[0]['cash']) < quote['price'] * int(request.form.get("shares")):
            return apology("not enough funds!")

         # run transaction on database
        db.execute("INSERT INTO transactions (userID, symbol, shares, price) VALUES(:userID, :symbol, :shares, :price)",
                   userID=session["user_id"], symbol=request.form.get("symbol").upper(),
                   shares=int(request.form.get("shares")), price=quote['price'])

        # update users cash
        db.execute("UPDATE users SET cash = :cash WHERE id = :id",
                   cash=result[0]['cash'] - quote['price'] * int(request.form.get("shares")),
                   id=session["user_id"])

        # redirect user to home page
        flash("Successfully bought {} shares of '{}'".format(request.form.get("shares"),
                                                             request.form.get("symbol")))
        return redirect(url_for("index"))

    # else if user reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("buy.html")


@app.route("/history")
@login_required
def history():
    """Show history of transactions."""

    # prepare transactions for display
    transactions = list(dict())
    rows = db.execute("SELECT * FROM transactions WHERE userID = :userID",
                      userID=session["user_id"])
    for row in rows:
        transactions.append({'symbol': row["symbol"], 'shares': row["shares"],
                             'price': row["price"], 'time': row["time"]})

    # display transactions
    return render_template("history.html", transactions=transactions)


@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in."""

    # forget any user_id
    session.clear()

    # if user reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # ensure username was submitted
        if not request.form.get("username"):
            return apology("must provide username")

        # ensure password was submitted
        elif not request.form.get("password"):
            return apology("must provide password")

        # query database for username
        rows = db.execute("SELECT * FROM users WHERE username = :username",
                          username=request.form.get("username"))

        # ensure username exists and password is correct
        if len(rows) != 1 or not ctx.verify(request.form.get("password"), rows[0]["hash"]):
            return apology("invalid username and/or password")

        # remember which user has logged in
        session["user_id"] = rows[0]["id"]

        # redirect user to home page
        flash("Logged in as {}".format(request.form.get("username")))
        return redirect(url_for("index"))

    # else if user reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out."""

    # forget any user_id
    session.clear()

    # redirect user to login form
    flash("Successfully logged out!")
    return redirect(url_for("login"))


@app.route("/quote", methods=["GET", "POST"])
@login_required
def quote():
    """Get stock quote."""

    # if user reached route via POST (as by submitting a form via POST)
    if request.method == "POST":
        # get quote for specified symbol
        quote = lookup(request.form.get("symbol").upper())

        # check that quote returned
        if quote == None:
            return apology("Could not find price of symbol")

        # display quote
        flash("A share of {} costs ${:.2f}".format(quote["symbol"], quote["price"]))
        return render_template("quoted.html", quote=quote)

    # else if user reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("quote.html")


@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user."""

    # forget any user_id
    session.clear()

    # if user reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # ensure username was submitted
        if not request.form.get("username"):
            return apology("must provide username")

        # ensure password was submitted
        elif not request.form.get("password"):
            return apology("must provide password")

        # ensure passwordConfirm was submitted
        elif not request.form.get("passwordConfirm"):
            return apology("must provide password confirmation")

        # ensure passwords match
        if request.form.get("password") != request.form.get("passwordConfirm"):
            return apology("must provide matching passwords")

        # encrypt password
        hashedPass = ctx.hash(request.form.get("password"))

        # insert username and password into db
        result = db.execute("INSERT INTO users (username, hash) VALUES(:username, :hash)",
                            username=request.form.get("username"), hash=hashedPass)

        # check for unique user
        if not result:
            return apology("username taken please try another")

        # remember which user has logged in (just registered)
        session["user_id"] = result

        # get default cash
        cash = db.execute("SELECT cash FROM users WHERE id = :id", id=session["user_id"])[0]["cash"]

        # redirect user to home page
        flash("Successfully registered. You have ${} to start investing!".format(cash))
        return redirect(url_for("index"))

    # else if user reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("register.html")


@app.route("/sell", methods=["GET", "POST"])
@login_required
def sell():
    """Sell shares of stock."""

    # if user reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # check that symbol is entered
        if not request.form.get("symbol"):
            return apology("must provide stock symbol")

        # check that num of shares entered (positive int)
        if not request.form.get("shares") or int(request.form.get("shares")) < 0:
            return apology("must provide a valid # of shares")

        # get price
        quote = lookup(request.form.get("symbol"))
        if quote == None:
            return apology("could not find price of symbol")

        # get users cash
        result = db.execute("SELECT * FROM users WHERE id = :id",
                            id=session["user_id"])

        # check that cash amount could be retrieved from db
        if result == None:
            return apology("could not get users cash")

        # get num of shares user currently owns
        numShares = 0
        rows = db.execute("SELECT * FROM transactions WHERE userID = :userID AND symbol = :symbol",
                          userID=session["user_id"], symbol=request.form.get("symbol"))
        for row in rows:
            numShares += row["shares"]

        # check that sale is possible
        if numShares < int(request.form.get("shares")):
            return apology("you can't sell that many shares")

        # run transaction on db
        db.execute("INSERT INTO transactions (userID, symbol, shares, price) VALUES(:userID, :symbol, :shares, :price)",
                   userID=session["user_id"], symbol=request.form.get("symbol").upper(),
                   shares=-int(request.form.get("shares")), price=quote['price'])

        # update users cash
        db.execute("UPDATE users SET cash = :cash WHERE id = :id",
                   cash=result[0]['cash'] + quote['price'] * int(request.form.get("shares")),
                   id=session["user_id"])

        # redirect user to home page
        flash("Successfully sold {} shares of '{}'".format(request.form.get("shares"),
                                                           request.form.get("symbol")))
        return redirect(url_for("index"))

    # else if user reached route via GET (as by clicking a link or via redirect)
    else:
        # get stock symbols that user has ownership in
        stocks = list()
        rows = db.execute("SELECT * FROM transactions WHERE userID = :userID",
                          userID=session["user_id"])
        for row in rows:
            if not row["symbol"] in stocks:
                stocks.append(row["symbol"])
        return render_template("sell.html", stocks=stocks)
