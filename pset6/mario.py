import cs50


def main():
    height = -1
    # Prompt user for height of pyramid until a non-negative integer <= 23 is inputted
    while (height < 0 or height > 23):
        height = getHeight()
    # Print the pyramid
    pyramid(height)


# print 'double half pyramid' of height h
def pyramid(h):
    # for each row of the pyramid
    for i in range(1, 1 + h):
        # calculate number of ' ' and '#'
        spaces = h - i
        hashes = h - spaces
        # left half
        printChars(spaces, ' ')
        printChars(hashes, '#')
        # 2 space division
        printChars(2, ' ')
        # right half
        printChars(hashes, '#')
        print()


# prints c n times
def printChars(n, c):
    for i in range(n):
        print(c, end='')


# Prompts user for height and returns an int
def getHeight():
    print("Height: ", end='')
    return cs50.get_int()


# End of file
if __name__ == '__main__':
    main()