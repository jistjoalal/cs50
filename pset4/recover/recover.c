/**
 * Recovers JPEG files from an SD card image
 */

#include <stdio.h>
#include <stdint.h>

int main(int argc, char *argv[])
{
    // ensure proper usage
    if (argc != 2)
    {
        fprintf(stderr, "Usage: ./recover image\n");
        return 1;
    }

    // remember card image filename
    char *infile = argv[1];

    // open input file
    FILE *image = fopen(infile, "r");
    if (image == NULL)
    {
        fprintf(stderr, "Could not open %s.\n", infile);
        return 2;
    }

    // keep track of jpgs found
    int found = 0;

    // temp storage for jpg being recovered
    FILE *jpg;

    // repeat until end of card
    int endOfCard = 0;
    while (!endOfCard)
    {
        // fill buffer with the next 512 bytes. if next block < 512, end of card.
        uint8_t buffer[512];
        if (!fread(&buffer, 512, 1, image))
        {
            endOfCard = 1;
        }
        // not end of card
        else
        {
            // check for start of jpeg
            if (buffer[0] == 0xff &&
                buffer[1] == 0xd8 &&
                buffer[2] == 0xff &&
                (buffer[3] & 0xf0) == 0xe0)
            {
                // make filename for new jpeg
                char filename[8];
                sprintf(filename, "%03i.jpg", found);

                // if this is the first jpg
                if (found == 0)
                {
                    // open first jpg
                    jpg = fopen(filename, "w");
                }
                else
                {
                    // close the jpg that is currently open
                    fclose(jpg);

                    // open new jpg
                    jpg = fopen(filename, "w");
                }

                found++;
            }

            // write to buffer except before the first jpg is found
            if (found > 0)
            {
                fwrite(&buffer, 512, 1, jpg);
            }
        }
    }

    // clean up
    fclose(jpg);
    fclose(image);
}