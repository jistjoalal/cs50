#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

char alphaShift(char _c, int _k);
bool isAlpha(char _c);

int main(int argc, string argv[])
{
    // error detection
    if (argc != 2)
    {
        printf("Usage: ./caesar k\n");
        return 1;
    }
    // store k value
    int k = atoi(argv[1]) % 26;
    // get input string from user
    printf("plaintext: ");
    string input = get_string();
    // print ciphertext
    printf("ciphertext: ");
    for (int i = 0, n = strlen(input); i < n; i++)
    {
        printf("%c", alphaShift(input[i], k));
    }
    printf("\n");
}

// shift an alphabetical char according to k (0-25)
char alphaShift(char _c, int _k)
{
    // if char is alphabetical
    if (isAlpha(_c))
    {
        // check for overflowing end of alphabet
        if (toupper(_c) + _k > 90)
        {
            return (_c + _k - 26);
        }
        else
        {
            return (_c + _k);
        }
    }
    // non-alphabetical return normal
    else
    {
        return _c;
    }
}

// return true if char is alphabetical
bool isAlpha(char _c)
{
    if (toupper(_c) >= 65 && toupper(_c) <= 90)
    {
        return true;
    }
    else
    {
        return false;
    }
}