/**
 * Declares a dictionary's functionality.
 */

#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <stdbool.h>
#include <stdlib.h>

// maximum length for a word
// (e.g., pneumonoultramicroscopicsilicovolcanoconiosis)
#define LENGTH 45
#define ALPHABET_SIZE 27

/**
 * Trie node for loading the dictionary
 */
typedef struct node
{
    bool isWord;
    struct node *children[ALPHABET_SIZE];
} node;

/**
 * Returns a new malloc'd and initialized trie node
 */
struct node *getNode(void);

/**
 * Recursively frees a trie structure of nodes
 */
void freeTrie(node *current);

/**
 * Gets the number of words stored in the trie
 */
int sizeTrie(node *current);

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char *word);

/**
 * Loads dictionary into memory. Returns true if successful else false.
 */
bool load(const char *dictionary);

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void);

/**
 * Unloads dictionary from memory.  Returns true if successful else false.
 */
bool unload(void);

#endif // DICTIONARY_H
