#include <stdio.h>
#include <cs50.h>

int main(void)
{
    // Prompt user for minutes spent in shower
    printf("Minutes: ");
    int minutes = get_int();
    // Print equivalent water consumption in bottles of water
    printf("Bottles: %d\n", minutes * 12);
}