#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

int main(void)
{
    // get input string from user
    string input = get_string();
    // print first char if its alphabetical
    if (input[0] > 65)
    {
        printf("%c", toupper(input[0]));
    }
    // iterate over each char in input string (starting with 1)
    for (int i = 1, n = strlen(input); i < n; i++)
    {
        // print char if alpha and preceded by a space
        if (input[i] > 65 && input[i - 1] == 32)
        {
            printf("%c", toupper(input[i]));
        }
    }
    printf("\n");
}