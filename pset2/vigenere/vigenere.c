#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

char alphaShift(char _c, int _k);
bool charIsAlpha(char _c);
bool strIsAlpha(string _s);

int main(int argc, string argv[])
{
    // error detection
    if (argc != 2 || !strIsAlpha(argv[1]))
    {
        printf("Usage: ./vigenere k\n");
        return 1;
    }
    // store k value
    string k = argv[1];
    int kLength = strlen(k);
    // get input string from user
    printf("plaintext: ");
    string input = get_string();
    // print ciphertext
    printf("ciphertext: ");
    // keep track of non alpha chars so they dont affect cipher
    int nonAlpha = 0;
    for (int i = 0, n = strlen(input); i < n; i++)
    {
        // shift char if alphabetical
        if (charIsAlpha(input[i]))
        {
            // nk is the individual shift of the char (0-25)
            int nk = toupper(k[(i - nonAlpha) % kLength]) - 65;
            // nk is used as _k in alphaShift from caesar cipher
            printf("%c", alphaShift(input[i], nk));
        }
        // otherwise just print, incrementing non alpha
        else
        {
            nonAlpha++;
            printf("%c", input[i]);
        }
    }
    printf("\n");
}

// shift an alphabetical char according to k (0-25)
char alphaShift(char _c, int _k)
{
    // if char is alphabetical
    if (charIsAlpha(_c))
    {
        // check for overflowing end of alphabet
        if (toupper(_c) + _k > 90)
        {
            return (_c + _k - 26);
        }
        else
        {
            return (_c + _k);
        }
    }
    // non-alphabetical return normal
    else
    {
        return _c;
    }
}

// return true if char is alphabetical
bool charIsAlpha(char _c)
{
    if (toupper(_c) >= 65 && toupper(_c) <= 90)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// returns false if any char in str is non alpha
bool strIsAlpha(string _s)
{
    for (int i = 0, n = strlen(_s); i < n; i++)
    {
        if (!charIsAlpha(_s[i]))
        {
            return false;
        }
    }
    return true;
}