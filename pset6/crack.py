import cs50
import sys
import math
import crypt


def main():
    # error checking
    if len(sys.argv) != 2:
        print("Usage: python crack.py hash")
        exit(1)

    # store hash and salt
    myHash = sys.argv[1]
    salt = myHash[:2]

    # for each possibility of num. of digits in key
    for digits in range(1, 5):
        # initialize test key to all A's
        testKey = list("A" * digits)
        # for all possible permutations of key given # of digits
        for i in range(1, 52 ** digits + 1):
            # compare test key hash to target hash
            if (crypt.crypt("".join(testKey), salt) == myHash):
                # give result to user and exit
                print("".join(testKey))
                exit(0)
            # for each digit check for incrementing char
            for j in range(0, digits):
                if (i % (52 ** j) == 0):
                    testKey[j] = nextChar(testKey[j])


def nextChar(c):
    if c == "Z":
        return "a"
    elif c == "z":
        return "A"
    else:
        return chr(ord(c) + 1)


# End of file
if __name__ == '__main__':
    main()