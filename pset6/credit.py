import cs50
import math


def main():
    number = -1
    # promt user for number until a (somewhat) valid one is enetered
    while number < 0:
        number = getNumber()
    # print the numbers type
    printType(number)
    exit(0)


# promts user for number returns as variable
def getNumber():
    print("Number: ", end='')
    return cs50.get_float()


# simply prints the type of cc if valid, else prints "INVALID"
def printType(cc):
    returnText = "INVALID"
    if validSum(cc):
        returnText = getType(cc)
    print(returnText)


# returns true if sum is valid, else false
def validSum(cc):
    digitSum = 0
    numDigits = math.log10(cc) + 1
    i = 2
    while i <= numDigits:
        digitDoubled = getDigitAt(cc, i) * 2
        if digitDoubled >= 10:
            digitSum += digitDoubled - 9
        else:
            digitSum += digitDoubled
        i += 2
    i = 1
    while i <= numDigits:
        digitSum += getDigitAt(cc, i)
        i += 2
    return digitSum % 10 == 0


# returns the credit card type in the form of string
def getType(cc):
    if getDigitAt(cc, 13) == 4 or getDigitAt(cc, 16) == 4:
        return "VISA"
    if getDigitAt(cc, 15) == 3 and (getDigitAt(cc, 14) == 4 or getDigitAt(cc, 14) == 7):
        return "AMEX"
    if getDigitAt(cc, 16) == 5 and (getDigitAt(cc, 15) > 0 and getDigitAt(cc, 15) < 6):
        return "MASTERCARD"


# gets digit of number at index i
def getDigitAt(number, i):
    lastNDigits = number % (10 ** i)
    lastNMinusOneDigits = number % (10 ** (i - 1))
    return (lastNDigits - lastNMinusOneDigits) / (10 ** (i - 1))


# End of file
if __name__ == '__main__':
    main()