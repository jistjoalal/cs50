/**
 * helpers.c
 *
 * Helper functions for Problem Set 3.
 */

#include <cs50.h>
#include <string.h>

#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n)
{
    int start = 0;
    int end = n - 1;
    int middle = 0;
    // while n > 0
    while (start <= end)
    {
        middle = (end + start) / 2;
        // if value is to the right
        if (value > values[middle])
        {
            start = middle + 1;
        }
        // if value is to the left
        else if (value < values[middle])
        {
            end = middle - 1;
        }
        // if value is in the middle
        else
        {
            return true;
        }
    }
    return false;
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    // initialize counter array for 0-65535 to all 0's
    int count[65536] = { 0 };

    // take tally of how often each number 0-65535 appears in values
    for (int i = 0; i < n; i++)
    {
        if (values[i] < 65536)
        {
            count[values[i]]++;
        }
    }
    int amountSorted = 0;
    int number = 0;
    // for each number in counter range while amountSorted < size of array to sort
    while (number < 65536 && amountSorted < n)
    {
        // if counter array found values of this index
        if (count[number] > 0)
        {
            // for the number of times it found each value
            for (int i = 0; i < count[number]; i++)
            {
                // move the value to its sorted position in the array
                values[amountSorted + i] = number;
                amountSorted++;
            }
        }
        number++;
    }
}
