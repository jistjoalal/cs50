/**
 * Implements a dictionary's functionality.
 */

#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

#include "dictionary.h"

// root node for trie structure
struct node *root;

/**
 * Returns a new malloc'd and initialized trie node
 */
struct node *getNode(void)
{
    // malloc a new node
    struct node *nNode = NULL;
    nNode = (struct node *)malloc(sizeof(struct node));
    // initialize node
    nNode->isWord = false;
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        nNode->children[i] = NULL;
    }
    return nNode;
}

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char *word)
{
    struct node *current = root;
    int letter;

    for (int i = 0, len = strlen(word); i < len; i++)
    {
        // index the letter 0-26 (26 = ')
        letter = (word[i] == '\'') ? 26 : (int)toupper(word[i]) - 65;

        // return false if link b/w current and next letter cant be found in trie
        if (current->children[letter] == NULL)
        {
            return false;
        }
        // move placeholder to next letter
        current = current->children[letter];
    }

    return (current != NULL && current->isWord);
}

/**
 * Loads dictionary into memory. Returns true if successful else false.
 * param: filename of dictionary
 */
bool load(const char *dictionary)
{
    // initialize root node of trie
    root = getNode();

    // open dictionary
    FILE *dict = fopen(dictionary, "r");
    if (dict == NULL)
    {
        fprintf(stderr, "Could not open %s.\n", dictionary);
        return 2;
    }

    // read each word line by line into the trie
    // +2 (fgets reads in '\n' and adds '\0')
    char buf[LENGTH + 2];
    while (fgets(buf, sizeof(buf), dict) != NULL)
    {
        // placeholder node for scanning the trie
        struct node *current = root;

        // for each letter in word
        int letter;
        for (int i = 0, len = strlen(buf); i < len - 1; i++)
        {
            // index the letter 0-26 (26 = ')
            letter = (buf[i] == '\'') ? 26 : (int)toupper(buf[i]) - 65;

            // make new node if needed and link to next letter
            if (!current->children[letter])
            {
                current->children[letter] = getNode();
            }

            // set placeholder to next letter in word
            current = current->children[letter];
        }

        // mark end of word
        current->isWord = true;
    }
    fclose(dict);
    return true;
}

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void)
{
    return sizeTrie(root);
}

/**
 * Unloads dictionary from memory. Returns true if successful else false.
 */
bool unload(void)
{
    freeTrie(root);
    return true;
}

/**
 * Gets the number of words stored in the trie
 */
int sizeTrie(node *current)
{
    // add word to count if found
    int result = current->isWord ? 1 : 0;

    // for each child do the same
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        if (current->children[i] != NULL)
        {
            result += sizeTrie(current->children[i]);
        }
    }
    return result;
}

// Recursively frees a trie structure of nodes
void freeTrie(node *current)
{
    // root case
    if (!current)
    {
        return;
    }
    // for each child
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        freeTrie(current->children[i]);
    }
    // base case
    free(current);
}
